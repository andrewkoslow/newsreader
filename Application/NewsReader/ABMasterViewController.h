//
//  ABMasterViewController.h
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ABMasterViewController : UIViewController <UITabBarDelegate, UITableViewDataSource>

@property (retain) IBOutlet UIView *welcomeView;
@property (retain) IBOutlet UITableView *tableView;
@property (retain) IBOutlet UIButton *reloadFeedButton;
@property (retain) NSURL *feedURL;

- (IBAction)reloadFeed:(id)sender;

@end
