//
//  ABDetailViewController.h
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABFeedItem.h"


@interface ABDetailViewController : UIViewController <UIWebViewDelegate>

@property (retain) IBOutlet UIWebView *feedContentView;
@property (retain) ABFeedItem *feedItem;

@end
