//
//  ABFeed.h
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ABFeed : NSObject

@property (retain) NSString *title;
@property (retain) NSArray *items;

@end
