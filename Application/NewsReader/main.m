//
//  main.m
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ABAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ABAppDelegate class]));
    }
}
