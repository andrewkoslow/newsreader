//
//  ABFeedItem.m
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import "ABFeedItem.h"


@implementation ABFeedItem

@synthesize title;
@synthesize link;
@synthesize publicationDate;


- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ title \"%@\" link \"%@\" publicationDate %@>", NSStringFromClass([self class]), self.title, self.link, self.publicationDate];
}


@end
