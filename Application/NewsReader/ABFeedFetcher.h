//
//  ABFeedFetcher.h
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import <Foundation/Foundation.h>


@class ABFeedFetcher;


@protocol ABFeedFetcherDelegate

- (void)feedFetcher:(ABFeedFetcher *)fetcher didFetchData:(NSData *)data;
- (void)feedFetcherDidFailFetchFeed:(ABFeedFetcher *)fetcher;

@end


@interface ABFeedFetcher : NSObject

@property (assign) id <ABFeedFetcherDelegate> delegate;

- (void)fetchFromURL:(NSURL *)feedURL;

@end
