//
//  ABMasterViewController.m
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import "ABMasterViewController.h"
#import "ABDetailViewController.h"
#import "ABFeedFetcher.h"
#import "ABFeedParser.h"
#import "ABFeedItem.h"


@interface ABMasterViewController () <ABFeedFetcherDelegate>

@property (retain) ABFeedFetcher *feedFetcher;
@property (retain) ABFeed *feed;

- (void)updateView;

@end


@implementation ABMasterViewController

@synthesize welcomeView;
@synthesize tableView;
@synthesize reloadFeedButton;
@synthesize feedURL;
@synthesize feedFetcher;
@synthesize feed;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self reloadFeed:self];
    
    UIViewController *welcomeViewController = [[UIViewController alloc] initWithNibName:@"ABWelcomeView" bundle:nil];
    [self presentModalViewController:welcomeViewController animated:NO];
}


- (void)viewWillAppear:(BOOL)animated {
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.feed.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    ABFeedItem *feedItem = [self.feed.items objectAtIndex:indexPath.row];
    cell.textLabel.text = feedItem.title;
    
    
    NSDate *sourceDate = feedItem.publicationDate;
    
    if (sourceDate) {
        NSTimeZone *sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
        
        NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
        NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
        NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
        
        NSDate *date = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
        
        cell.detailTextLabel.text = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];
    } else {
        cell.detailTextLabel.text = @"";
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ABDetailViewController *detailViewController = [[ABDetailViewController alloc] initWithNibName:@"ABDetailViewController" bundle:nil];
    detailViewController.feedItem = [self.feed.items objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:detailViewController animated:YES];
}


- (void)feedFetcher:(ABFeedFetcher *)fetcher didFetchData:(NSData *)data {
    ABFeedParser *parser = [ABFeedParser new];
    ABFeed *newFeed = [parser parseFeedFromData:data];
    
    self.feedFetcher = nil;
    
    if (newFeed != nil) {
        self.feed = newFeed;
        [self updateView];
    }
}


- (void)feedFetcherDidFailFetchFeed:(ABFeedFetcher *)fetcher {
    self.feedFetcher = nil;
    
    [self updateView];
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MasterView.FeedFetchFailedAlert.Title", @"")
                                message:NSLocalizedString(@"MasterView.FeedFetchFailedAlert.Message", @"")
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"MasterView.FeedFetchFailedAlert.Button", @"")
                      otherButtonTitles:nil] show];
}


- (void)updateView {
    if (self.modalViewController) {
        [self.modalViewController dismissModalViewControllerAnimated:NO];
    }
    
    if (self.feed.title.length > 0) {
        self.title = self.feed.title;
    } else {
        self.title = NSLocalizedString(@"MasterView.EmptyTitle", @"");
    }
    
    [self.tableView reloadData];
    
    self.reloadFeedButton.enabled = YES;
}


- (IBAction)reloadFeed:(id)sender {
    if (self.feedFetcher == nil) {
        self.reloadFeedButton.enabled = NO;
        
        self.feedFetcher = [ABFeedFetcher new];
        self.feedFetcher.delegate = self;
        [self.feedFetcher fetchFromURL:self.feedURL];
    }
}


@end
