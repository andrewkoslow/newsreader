//
//  ABFeedItem.h
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ABFeedItem : NSObject

@property (retain) NSString *title;
@property (retain) NSURL *link;
@property (retain) NSDate *publicationDate;

@end
