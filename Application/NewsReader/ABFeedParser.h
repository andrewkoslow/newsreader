//
//  ABFeedParser.h
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABFeed.h"


@interface ABFeedParser : NSObject

- (ABFeed *)parseFeedFromData:(NSData *)sourceData;

@end
