//
//  ABFeedParser.m
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import "ABFeedParser.h"
#import "ABFeedItem.h"


@interface ABFeedParser () <NSXMLParserDelegate>

@property (retain) ABFeed *feed;
@property (retain) NSMutableArray *feedItems;
@property (retain) ABFeedItem *currentItem;
@property (retain) NSMutableString *currentElementStringValue;

@end


@implementation ABFeedParser

@synthesize feed;
@synthesize feedItems;
@synthesize currentItem;
@synthesize currentElementStringValue;


- (ABFeed *)parseFeedFromData:(NSData *)sourceData {
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:sourceData];
    parser.delegate = self;
    parser.shouldResolveExternalEntities = YES;
    [parser parse];
    
    ABFeed *resultFeed = self.feed;
    self.feed = nil;
    
    return resultFeed;
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    self.currentElementStringValue = nil;
    
    if ([elementName isEqualToString:@"title"]) {
        if (self.feed == nil) {
            NSAssert(self.currentItem == nil, @"self.currentItem == nil");
            
            self.feed = [ABFeed new];
            self.feedItems = [NSMutableArray new];
        }
    } else if ([elementName isEqualToString:@"item"] && self.currentItem == nil) {
        NSAssert(self.feedItems != nil, @"self.feedItems != nil");
        
        self.currentItem = [ABFeedItem new];
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (self.currentElementStringValue == nil) {
        self.currentElementStringValue = [NSMutableString new];
    }
    
    [self.currentElementStringValue appendString:string];
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"title"]) {
        if (self.currentItem != nil) {
            self.currentItem.title = [NSString stringWithString:self.currentElementStringValue];
        } else if (self.feed != nil) {
            self.feed.title = [NSString stringWithString:self.currentElementStringValue];
        }
    } else if ([elementName isEqualToString:@"link"]) {
        if (self.currentItem != nil) {
            self.currentItem.link = [NSURL URLWithString:self.currentElementStringValue];
        }
    } else if ([elementName isEqualToString:@"pubDate"]) {
        if (self.currentItem != nil) {
            NSDateFormatter *dateFormat = [NSDateFormatter new];
            dateFormat.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
            dateFormat.dateFormat = @"EE, d MM yyyy HH:mm:ss zzz";
            dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            
            self.currentItem.publicationDate = [dateFormat dateFromString:self.currentElementStringValue];
        }
    } else if ([elementName isEqualToString:@"item"] && self.currentItem != nil) {
        [self.feedItems addObject:self.currentItem];
        self.currentItem = nil;
    }
    
    self.currentElementStringValue = nil;
}


- (void)parserDidEndDocument:(NSXMLParser *)parser {
    if (self.feed != nil && self.feedItems != nil) {
        self.feed.items = [NSArray arrayWithArray:self.feedItems];
        self.feedItems = nil;
    }
}


@end
