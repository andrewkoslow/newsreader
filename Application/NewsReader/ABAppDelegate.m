//
//  ABAppDelegate.m
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import "ABAppDelegate.h"
#import "ABMasterViewController.h"


@implementation ABAppDelegate

@synthesize window;
@synthesize navigationController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    ABMasterViewController *masterViewController = [[ABMasterViewController alloc] initWithNibName:@"ABMasterViewController" bundle:nil];
    masterViewController.feedURL = [NSURL URLWithString:@"http://rss.cnn.com/rss/cnn_topstories.rss"];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    
    return YES;
}


@end
