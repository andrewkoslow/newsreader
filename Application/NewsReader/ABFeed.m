//
//  ABFeed.m
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import "ABFeed.h"


@implementation ABFeed

@synthesize title;
@synthesize items;


- (NSString *)description {
    return [NSString stringWithFormat:@"<%@ title \"%@\" items %@>", NSStringFromClass([self class]), self.title, self.items];
}


@end
