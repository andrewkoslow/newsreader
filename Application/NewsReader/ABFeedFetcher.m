//
//  ABFeedFetcher.m
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import "ABFeedFetcher.h"


@interface ABFeedFetcher () <NSURLConnectionDelegate>

@property (retain) NSURLConnection *connection;
@property (retain) NSMutableData *data;

@end


@implementation ABFeedFetcher

@synthesize delegate;
@synthesize connection;
@synthesize data;


- (void)fetchFromURL:(NSURL *)feedURL {
    NSAssert(feedURL != nil, @"feedURL != nil");
    NSAssert(self.connection == nil, @"self.connection == nil");
    NSAssert(self.data == nil, @"self.data == nil");
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        @autoreleasepool {
            NSURLRequest *feedRequest = [NSURLRequest requestWithURL:feedURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0];
            
            self.connection = [[NSURLConnection alloc] initWithRequest:feedRequest delegate:self startImmediately:NO];
            
            if (self.connection) {
                self.data = [NSMutableData new];
                [self.connection start];
                
                CFRunLoopRun();
            } else {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.delegate feedFetcherDidFailFetchFeed:self];
                });
            }
        }
    });
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSAssert(self.data != nil, @"self.data != nil");
    
    [self.data setLength:0];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)receivedData {
    NSAssert(self.data != nil, @"self.data != nil");
    
    [self.data appendData:receivedData];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.delegate feedFetcher:self didFetchData:self.data];
    });
    
    [self reset];
    
    CFRunLoopStop(CFRunLoopGetCurrent());
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.delegate feedFetcherDidFailFetchFeed:self];
    });
    
    [self reset];
    
    CFRunLoopStop(CFRunLoopGetCurrent());
}


- (void)reset {
    self.connection = nil;
    self.data = nil;
}


@end
