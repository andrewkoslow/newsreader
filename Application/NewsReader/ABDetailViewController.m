//
//  ABDetailViewController.m
//  NewsReader
//
//  Created by Andrey A. Kozlov on 12.05.12.
//  Copyright (c) 2012 Andrey A. Kozlov. All rights reserved.
//

#import "ABDetailViewController.h"


@implementation ABDetailViewController

@synthesize feedItem;
@synthesize feedContentView;


#pragma mark - Managing the detail item

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated {
    self.title = self.feedItem.title;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:self.feedItem.link];
    [self.feedContentView loadRequest:request];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"DetailView.ContentLoadFailedAlert.Title", @"")
                                message:error.localizedDescription
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"DetailView.ContentLoadFailedAlert.Button", @"")
                      otherButtonTitles:nil] show];
}


@end
